﻿using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailTravelDBCleaner
{
    public static class DBCleaner
    {
        internal static void DeleteOldMails()
        {
            try
            {
                DateTime date = DateTime.Today.AddDays(-1);
                EmailOut_Services eService = new EmailOut_Services();
                eService.Delete(date);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
