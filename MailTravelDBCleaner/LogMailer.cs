﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MailTravelDBCleaner
{
    public static class LogMailer
    {
        public static void MailLog()
        {
            String logPath = "C:\\BackupLog\\" + DateTime.Now.ToString("yyyy-MM-dd") + "DBCleaner.log";
            StringBuilder mailbody = new StringBuilder();
            mailbody.Append("Beste,\n\nOp " + DateTime.Now.ToShortDateString() + " is er een poging gedaan om de Mailtravel database op te kuisen.\n");
            mailbody.Append("Dit is mislukt.\n");
            mailbody.Append("Kijk in het mee gezonden logbestand voor meer informatie.\n\n");
            mailbody.Append("This is an automatic mail send from the mail travel cleaner service.\n");
            mailbody.Append("Do not reply to this mail.");
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add("stefan.k@itc.be");
            message.Subject = "Log van de database cleaner service " + DateTime.Now.ToString("dd-MM-yyyy");
            message.From = new System.Net.Mail.MailAddress("info@itc.be", "ITC Sharepoint");
            message.IsBodyHtml = false;
            message.Body = mailbody.ToString();
            System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(logPath);
            message.Attachments.Add(att);
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.outlook.office365.com", 587); //587

            smtp.EnableSsl = true;
            smtp.Credentials = new System.Net.NetworkCredential("info@itc.be", "@lgemeen@ITC");

            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                smtp.Send(message);
            }

            catch (Exception ex)
            {
                echo(ex.Message);
            }
        }

        public static void echo(String t)
        {
            Console.WriteLine(t);
            String[] tekst = t.Split('\n');
            if (System.IO.File.Exists(@"C:\BackupLog\" + DateTime.Now.ToString("yyyy-MM-dd") + "DBCleaner.log"))
            {
                using (FileStream log = new FileStream(@"C:\BackupLog\" + DateTime.Now.ToString("yyyy-MM-dd") + "DBCleaner.log", FileMode.Append))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine(myT);
                    }
                    sw.Close();
                }
            }
            else
            {
                using (FileStream log = new FileStream(@"C:\BackupLog\" + DateTime.Now.ToString("yyyy-MM-dd") + "DBCleaner.log", FileMode.OpenOrCreate))
                {
                    StreamWriter sw = new StreamWriter(log);
                    foreach (String myT in tekst)
                    {
                        sw.WriteLine(myT);
                    }
                    sw.Close();
                }
            }
        }
    }
}
