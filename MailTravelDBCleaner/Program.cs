﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailTravelDBCleaner
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DBCleaner.DeleteOldMails();
            }
            catch ( Exception ex)
            {
                LogMailer.echo(ex.Message);
                LogMailer.MailLog();
            }           
        }
    }
}
