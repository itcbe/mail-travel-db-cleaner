﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class EmailOut_Services
    {
        public void Delete(DateTime date)
        {
            using (var entities = new DBEntities())
            {
                try
                {
                    var items = (from m in entities.EmailOut
                                 where m.DateTime < date
                                 select m).ToList();

                    if (items.Count > 0)
                    {
                        foreach (EmailOut item in items)
                        {
                            entities.EmailOut.Remove(item);
                        }
                        entities.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Fout bij het opkuisen van de DB!\n" + ex.Message);
                }
            }
        }
    }
}
